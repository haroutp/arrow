﻿using System;

namespace Arrow
{
    class Program
    {
        static void Main(string[] args)
        {
            ArrowClass ac = new ArrowClass(){
                Length = 23
            };

            ac.DrawArrow();
        }
    }
}
