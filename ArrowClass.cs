using System;

namespace Arrow
{
    class ArrowClass
    {
        private int length;

        public int Length { get => length; set => length = value; }

        public void DrawArrow(){
            for (int i = 1; i <= Length; i++)
            {
                if(i == Length){
                    System.Console.Write(">");
                }else{
                    System.Console.Write("-");
                }
            }
            System.Console.WriteLine();
        }
        
    }
}
